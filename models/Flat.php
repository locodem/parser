<?php

declare(strict_types=1);

namespace app\models;

use yii\db\ActiveRecord;

class Flat extends ActiveRecord
{
    public $flatNumber;
    public $totalArea;
    public $pricePerMeter;
    public $fullPrice;
    public $status;
    public $roomsAmount;
    public $entrance;

    public static function tableName()
    {
        return 'flats';
    }

    /**
     * @return mixed
     */
    public function getFlatNumber()
    {
        return $this->flatNumber;
    }

    /**
     * @param mixed $flatNumber
     * @return Flat
     */
    public function setFlatNumber($flatNumber)
    {
        $this->flatNumber = $flatNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalArea()
    {
        return $this->totalArea;
    }

    /**
     * @param mixed $totalArea
     * @return Flat
     */
    public function setTotalArea($totalArea)
    {
        $this->totalArea = $totalArea;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricePerMeter()
    {
        return $this->pricePerMeter;
    }

    /**
     * @param mixed $pricePerMeter
     * @return Flat
     */
    public function setPricePerMeter($pricePerMeter)
    {
        $this->pricePerMeter = $pricePerMeter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullPrice()
    {
        return $this->fullPrice;
    }

    /**
     * @param mixed $fullPrice
     * @return Flat
     */
    public function setFullPrice($fullPrice)
    {
        $this->fullPrice = $fullPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Flat
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomsAmount()
    {
        return $this->roomsAmount;
    }

    /**
     * @param mixed $roomsAmount
     * @return Flat
     */
    public function setRoomsAmount($roomsAmount)
    {
        $this->roomsAmount = $roomsAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntrance()
    {
        return $this->entrance;
    }

    /**
     * @param mixed $entrance
     * @return Flat
     */
    public function setEntrance($entrance)
    {
        $this->entrance = $entrance;
        return $this;
    }
}