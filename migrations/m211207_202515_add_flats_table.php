<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211207_202515_add_flats_table
 */
class m211207_202515_add_flats_table extends Migration
{
    public function up()
    {
        $this->createTable('flats', [
            'id' => Schema::TYPE_PK,
            'entrance' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_STRING,
            'rooms_amount' => Schema::TYPE_INTEGER,
            'flat_number' => Schema::TYPE_INTEGER,
            'total_area' => Schema::TYPE_FLOAT,
            'price_per_meter' => Schema::TYPE_FLOAT,
            'full_price' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        $this->dropTable('flats');
    }
}
