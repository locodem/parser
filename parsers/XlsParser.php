<?php

declare(strict_types=1);

namespace app\parsers;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class XlsParser
{
    /**
     * @param string $searchValue
     * @param Worksheet $worksheet
     * @return string[]
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function findCellsCoordinatesByValue(string $searchValue, Worksheet $worksheet): array
    {
        $cellsCoordinates = [];

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) {
                if ($cell->getValue() == $searchValue) {
                    $cellsCoordinates[] = $cell->getCoordinate();
                }
            }
        }

        return $cellsCoordinates;
    }

    /**
     * @param string $searchValue
     * @param Worksheet $worksheet
     * @return Cell[]
     */
    /* поиск ячейки по значению  TODO probably this method doesn't make sense */
    private function findCellsByValue(string $searchValue, Worksheet $worksheet): array
    {
        $cellsList = [];

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) {
                if ($cell->getValue() == $searchValue) {
                    $cellsList[] = $cell;
                }
            }
        }

        return $cellsList;
    }

    private function splitCellCoordinatesStringToIntegersList(array $cellCoordinatesList): array
    {
        $cellsCoordinatesAsIntegersList = [];
        foreach ($cellCoordinatesList as $cellCoordinates) {
            $cellsCoordinatesAsIntegersList[] = $this->getCellCoordinatesAsInteger($cellCoordinates);
        }

        return $cellsCoordinatesAsIntegersList;
    }

    private function getCellCoordinatesAsInteger(string $cellCoordinates): array
    {
        $indexes = [
            'A' => 1,
            'B' => 2,
            'C' => 3,
            'D' => 4,
            'E' => 5,
            'F' => 6,
            'G' => 7,
            'H' => 8,
            'I' => 9,
            'J' => 10,
            'K' => 11,
            'L' => 12,
            'M' => 13,
            'N' => 14,
            'O' => 15,
            'P' => 16,
            'Q' => 17,
            'R' => 18,
            'S' => 19,
            'T' => 20,
            'U' => 21,
            'V' => 22,
            'W' => 23,
            'X' => 24,
            'Y' => 25,
            'Z' => 26,
            'AA' => 27,
            'AB' => 28,
            'AC' => 29,
            'AD' => 30,
            'AE' => 31,
            'AF' => 32,
            'AG' => 33,
            'AH' => 34,
            'AI' => 35,
            'AJ' => 36,
            'AK' => 37,
            'AL' => 38,
            'AM' => 39,
            'AN' => 40,
            'AO' => 41,
            'AP' => 42,
            'AQ' => 43,
            'AR' => 44,
            'AS' => 45,
            'AT' => 46,
            'AU' => 47,
            'AV' => 48,
            'AW' => 49,
            'AX' => 50,
            'AY' => 51,
            'AZ' => 52,
            // from A to AZ
        ];
        $cellSplitCoordinates = [];
        if (preg_match('/^([A-Z]+)([0-9]+)$/i', $cellCoordinates, $matches)) {
            $cellSplitCoordinates = [
                'columnIndex' => $matches[1],
                'rowIndex' => (int)$matches[2]
            ];
        }

        $columnIndexAsInteger = $indexes[$cellSplitCoordinates['columnIndex']];
        $cellSplitCoordinates['columnIndex'] = $columnIndexAsInteger;

        return $cellSplitCoordinates;
    }

    /**
     * @param array $cellCoordinates
     * @param Worksheet $worksheet
     * @return Cell[]
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function getCellsWithFloorsList(array $cellCoordinates, Worksheet $worksheet): array
    {
        $cellsWithFloorsList = [];
        $cellWithMaxFloor = $worksheet->getCellByColumnAndRow(
            $cellCoordinates['columnIndex'],
            $cellCoordinates['rowIndex'] + 2
        );
        $maxFloorValue = $cellWithMaxFloor->getValue();
        $cellsWithFloorsList[$cellWithMaxFloor->getCoordinate()] = $cellWithMaxFloor->getValue();

        $nextFloorCell = [];
        for ($i = $maxFloorValue; $i >= 2;) {
            if ($nextFloorCell === []) {
                $nextFloorCell = $cellWithMaxFloor;
                $nextFloorCellCoordinatesAsInteger = $this->getCellCoordinatesAsInteger($nextFloorCell->getCoordinate());
                $cellWithFloorNumber = $worksheet->getCellByColumnAndRow(
                    $nextFloorCellCoordinatesAsInteger['columnIndex'],
                    $nextFloorCellCoordinatesAsInteger['rowIndex'] + 3
                );
                $cellsWithFloorsList[$cellWithFloorNumber->getCoordinate()] = $cellWithFloorNumber->getValue();
            } else {
                $nextFloorCellCoordinatesAsInteger = $this-> getCellCoordinatesAsInteger($nextFloorCell->getCoordinate());
                $cellWithFloorNumber = $worksheet->getCellByColumnAndRow(
                    $nextFloorCellCoordinatesAsInteger['columnIndex'],
                    $nextFloorCellCoordinatesAsInteger['rowIndex'] + 3
                );
                $cellsWithFloorsList[$cellWithFloorNumber->getCoordinate()] = $cellWithFloorNumber->getValue();
            }
            $i--;
        }

        return $cellsWithFloorsList;
    }

    private function getFlatNumber(array $cellCoordinates, Worksheet $worksheet, $step): int
    {
        $cellWithFlatNumber = $worksheet->getCellByColumnAndRow(
            $cellCoordinates['columnIndex'] + $step,
            $cellCoordinates['rowIndex']
        );

        return (int) $cellWithFlatNumber->getCalculatedValue();
    }

    private function getCellWithFlatNumberCoordinates(array $cellCoordinates, $step): array
    {
        return [
            'columnIndex' => $cellCoordinates['columnIndex'] + $step,
            'rowIndex' => $cellCoordinates['rowIndex'],
        ];
    }

    private function getTotalArea(array $cellCoordinates, Worksheet $worksheet): float
    {
        $cellWithTotalArea = $worksheet->getCellByColumnAndRow(
            $cellCoordinates['columnIndex'] + 1,
            $cellCoordinates['rowIndex'] - 1
        );

        return $cellWithTotalArea->getCalculatedValue();
    }

    private function getPricePerMeter(array $cellCoordinates, Worksheet $worksheet): int
    {
        $cellWithPricePerMeter = $worksheet->getCellByColumnAndRow(
            $cellCoordinates['columnIndex'] + 1,
            $cellCoordinates['rowIndex']
        );

        return $cellWithPricePerMeter->getCalculatedValue();
    }

    private function getFlatStatus(array $cellCoordinates, Worksheet $worksheet)
    {
        /* TODO rename $cellCoordinates to $cellWithFloorCoordinates */
        $cellUnderFlatNumber = $worksheet->getCellByColumnAndRow(
            $cellCoordinates['columnIndex'],
            $cellCoordinates['rowIndex'] - 1
        );

        $color = $worksheet->getStyle($cellUnderFlatNumber->getCoordinate())->getFill()->getStartColor()->getRGB();
        return $color === '000000' ? 'available' : 'sold';
    }

    private function getRoomsAmount($columnIndex, $rowIndex, Worksheet $worksheet): int
    {
        $cellWithRoomsAmount = $worksheet->getCellByColumnAndRow(
            $columnIndex,
            $rowIndex
        );

        $roomsCount = str_replace('к.кв', '', $cellWithRoomsAmount->getValue());

        return (int) trim($roomsCount);
    }

    private function getEntrance(array $baseCellCoordinates, Worksheet $worksheet): string
    {
        $i = 1;
        while (true) {
            $cell = $worksheet->getCellByColumnAndRow(
                $baseCellCoordinates['columnIndex'] + $i,
                $baseCellCoordinates['rowIndex'] - 1
            );

            if ($cell->getValue() !== null) {
                return $cell->getValue();
            }

            $i++;
        }
    }

    private function getStoyakAmount(array $baseCellCoordinates, Worksheet $worksheet): int
    {
        $i = 1;
        $status = true;
        $cellsWithRoomsAmountList = [];
        while ($status === true) {
            $cellWithRoomsAmount = $worksheet->getCellByColumnAndRow(
                $baseCellCoordinates['columnIndex'] + $i,
                $baseCellCoordinates['rowIndex']
            );

            if ($cellWithRoomsAmount->getValue() !== null && strpos($cellWithRoomsAmount->getValue(), 'к.кв') !== false) {
                $cellsWithRoomsAmountList[] = $cellWithRoomsAmount->getCoordinate();
            } else {
                $status = false;
            }

            $i = $i + 2;
        }

        return count($cellsWithRoomsAmountList);
    }

    private function getFileExtension(string $filename): string
    {
        return ucfirst(pathinfo($filename, PATHINFO_EXTENSION));
    }

    public function parse(string $filename): array
    {
        $flatsList = [];
        $reader = IOFactory::createReader($this->getFileExtension($filename));
        $spreadsheet = $reader->load($filename);
        $sheetAmount = $spreadsheet->getSheetCount();

        for ($sheetIndex = 0; $sheetIndex < $sheetAmount; $sheetIndex++) {
            $worksheet = $spreadsheet->getSheet($sheetIndex);
            $baseCellsCoordinatesListByValue = $this->findCellsCoordinatesByValue('эт.', $worksheet);
            $baseCellsCoordinatesAsIntegersList = $this->splitCellCoordinatesStringToIntegersList($baseCellsCoordinatesListByValue);

            foreach ($baseCellsCoordinatesAsIntegersList as $baseCellCoordinates) {
                $stoyakAmount = $this->getStoyakAmount($baseCellCoordinates, $worksheet);
                $cellsWithFloorsList = $this->getCellsWithFloorsList($baseCellCoordinates, $worksheet);

                foreach ($cellsWithFloorsList as $cellWithFloorCoordinate => $cellWithFloorObject) {

                    $step = 1;
                    $cellCoordinates = $this->getCellCoordinatesAsInteger($cellWithFloorCoordinate);
                    for ($i = 1; $i <= $stoyakAmount; $i++) {
                        /* TODO get FlatData */
                        $flatNumber = $this->getFlatNumber($cellCoordinates, $worksheet, $step);
                        $cellWithFlatNumber = $this->getCellWithFlatNumberCoordinates($cellCoordinates, $step);

                        if ($flatNumber === 0) {
                            continue;
                        }

                        $totalArea = $this->getTotalArea($cellWithFlatNumber, $worksheet);
                        $pricePerMeter = $this->getPricePerMeter($cellWithFlatNumber, $worksheet);
                        $entrance = $this->getEntrance($baseCellCoordinates, $worksheet);
                        $roomsAmount = $this->getRoomsAmount(
                            $cellWithFlatNumber['columnIndex'],
                            $baseCellCoordinates['rowIndex'],
                            $worksheet
                        );

                        $flatsList[$worksheet->getTitle()][$entrance][$flatNumber] = [
                            'totalArea' => $totalArea,
                            'pricePerMeter' => $pricePerMeter,
                            'fullPrice' => (int) ($totalArea * $pricePerMeter),
                            'status' => $this->getFlatStatus($cellWithFlatNumber, $worksheet),
                            'roomsAmount' => $roomsAmount,
                        ];

                        $step = $step + 2;
                    }
                }
            }
        }

        return $flatsList;
    }
}
