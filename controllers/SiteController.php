<?php

namespace app\controllers;

use app\models\Flat;
use app\parsers\XlsParser;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @var XlsParser $parser
     */
    private $parser;

    public function __construct($id, $module, $config = [])
    {
        $this->parser = new XlsParser();
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $entrance = 'Подъезд 3';
        $flatsList = $this->parser->parse('Shahmatka.xls');
        $flatsList = $flatsList['Лист1'][$entrance];

        foreach ($flatsList as $flatNumber => $flatData) {
            $flat = new Flat([
                'entrance' => $entrance,
                'rooms_amount' => $flatData['roomsAmount'],
                'status' => $flatData['status'],
                'flat_number'=> $flatNumber,
                'total_area' => $flatData['totalArea'],
                'price_per_meter' => $flatData['pricePerMeter'],
                'full_price' => $flatData['fullPrice']
            ]);
            $flat->save(false);
        }
        var_dump('flats was added to DB');
    }

    public function actionShow()
    {
        $flatsList = Flat::find()->all();

        if ($flatsList === null) {
            throw new \Exception('flats doesnt exists in DB');
        }

        return $this->render('show', [
            'flatList' => $flatsList,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
